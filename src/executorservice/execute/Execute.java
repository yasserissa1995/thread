package executorservice.execute;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Execute {

    private List<Runnable> runnables;

    public void createThreads() {

    /*   Runnable t = new Runnable() {
            @Override
            public void run() {
                System.out.println(Thread.currentThread().getName());
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        */


        Runnable t1 = new Thread(() -> {
            System.out.println("name from execute :: " + Thread.currentThread().getName());
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        Runnable t2 = new Thread(() -> {
            System.out.println("name from execute :: " + Thread.currentThread().getName());
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });


        Runnable t3 = new Thread(() -> {
            System.out.println("name from execute :: " + Thread.currentThread().getName());
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });


        Runnable t4 = new Thread(() -> {
            System.out.println("name from execute :: " + Thread.currentThread().getName());
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });

        runnables = Arrays.asList(t1, t2, t3, t4);
    }

    public void execute() {

        // tow thread run at the same time


        // single 5thread
      //  ExecutorService ee = Executors.newSingleThreadExecutor();
     //   ExecutorService eee = Executors.newCachedThreadPool();
    //    ExecutorService eeee = Executors.newWorkStealingPool(2);
        ExecutorService executorService = Executors.newFixedThreadPool(2);

        runnables.forEach(o -> executorService.execute(o));

        executorService.shutdown();

    }



}
