package executorservice.submit;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

public class Submit {

    private List<Callable<String>> callables;

    public void createThreads() {


        Callable<String> c1 = new Callable<String>() {
            @Override
            public String call() throws Exception {
                return Thread.currentThread().getName();
            }
        };


        Callable<String> c2 = new Callable<String>() {
            @Override
            public String call() throws Exception {
                return Thread.currentThread().getName();
            }
        };


        Callable<String> c3 = new Callable<String>() {
            @Override
            public String call() throws Exception {
                return Thread.currentThread().getName();
            }
        };


        Callable<String> c4 = new Callable<String>() {
            @Override
            public String call() throws Exception {
                return Thread.currentThread().getName();
            }
        };


        callables = Arrays.asList(c1, c2, c3, c4);
    }

    public void submit() {

        // tow thread run at the same time
        ExecutorService executorService = Executors.newFixedThreadPool(2);
        List<String> names = new ArrayList<>();

        callables.forEach(o -> {

            try {

                Future<String> name = executorService.submit(o);
                System.out.println("name from submit :: "+ name.get());
                names.add(name.get());

            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        });

        // print returned data
        System.out.println(names);

        executorService.shutdown();

    }

}
