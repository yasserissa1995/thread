import executorservice.execute.Execute;
import executorservice.submit.Submit;

public class Main {

    public static void main(String a[]){

        submit();
        execute();
    }

    private static void submit() {

        Submit basic = new Submit();

        basic.createThreads();
        basic.submit();
    }

    private static void execute() {

        Execute basic = new Execute();

        basic.createThreads();
        basic.execute();
    }



}
